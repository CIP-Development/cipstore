﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Reflection;

namespace AssemblyInfo
{
    class Program
    {
        static int Main(string[] args)
        {
            try
            {
                //string path = "\\Program Files\\ciptclpocket\\CIPTCLPocket2.exe";
                string appInstallPath = args[0];
                string appVersionInput = args[1];
                string appVersionOutput = args[2];

                Assembly SampleAssembly = Assembly.LoadFrom(appInstallPath + appVersionInput);
                var version = SampleAssembly.GetName().Version.ToString();

                using (StreamWriter sw = new StreamWriter(appInstallPath + appVersionOutput))
                {
                    sw.Write(version);
                }
                return 0;
            }
            catch 
            {
                return 1;
            }
        }
    }
}
