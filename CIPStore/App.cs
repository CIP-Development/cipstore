﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using Newtonsoft.Json;
using System.Collections;

namespace CIPStore
{
    [JsonObject(MemberSerialization.OptIn)]
    public class App
    {
        [JsonProperty]
        public string AppId { get; set; }

        [JsonProperty]
        public string AppName { get; set; }
        
        [JsonProperty]
        public string DefaultInstallPath { get; set; }
        
        [JsonProperty]
        public string DefaultVersionPath { get; set; }
        
        [JsonProperty]
        public string LastVersion { get; set; }

        [JsonProperty]
        public string LastVersionPath { get; set; }

        [JsonProperty]
        public List<string> LastVersionFiles { get; set; }

        [JsonProperty]
        public string LastInstaller { get; set; }

        [JsonProperty]
        public string AppIcon { get; set; }

        [JsonProperty]
        public bool ForcedUpdate { get; set; }

        public AppStatusEnum Status { get; set; }
        public int Order { get; set; }
        public string CurrentVersion { get; set; }

        public System.Drawing.Image Icon { get; set; }
    }

    public class sortAppHelper : IComparer<App>
    {
        public int Compare(App a, App b)
        {
            if (a.Order > b.Order)
                return 1;
            if (a.Order < b.Order)
                return -1;
            else
                return 0;
        }
    }

}
