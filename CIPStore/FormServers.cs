﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace CIPStore
{
    public partial class FormServers : Form
    {
        public string ServerPath { get; set; }

        public FormServers()
        {
            InitializeComponent();
        }

        private void FormServers_Load(object sender, EventArgs e)
        {
            listBox_Servers.SelectedIndex = 0;
        }

        private void button_Cancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void button_Select_Click(object sender, EventArgs e)
        {
            ServerPath = listBox_Servers.SelectedItem.ToString();
            this.Close();
        }

        private void button_TestConnection_Click(object sender, EventArgs e)
        {
            string URL = listBox_Servers.SelectedItem.ToString();
            webBrowser1.Url = new Uri(URL);
        }
    }
}