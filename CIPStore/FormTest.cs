﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Net;
using System.Net.Sockets;
using System.IO;
using System.Runtime.InteropServices;

namespace CIPStore
{
    public partial class FormTest : Form
    {
        public FormTest()
        {
            InitializeComponent();
        }

        private void button_Connect_Click(object sender, EventArgs e)
        {
            try
            {

                /*var ip = Dns.GetHostEntry("cipfs");
                MessageBox.Show(string.Format("{0},{1}", ip.HostName, ip.AddressList[0].ToString()));
                */

                NetworkConnection conn;
                NetworkCredential cred = new NetworkCredential("cvelasquez", "", "cgiarad");
                //foreach (var path in listBox_URN.Items)
                //{
                string dir = listBox_URN.SelectedItem.ToString();
                    using (conn = new NetworkConnection(dir, cred))
                    {
                        string[] fileEntries = Directory.GetFiles(dir);
                        MessageBox.Show(fileEntries.Length.ToString());
                    }
                //}
                

                //string[] theFolders = System.IO.Directory.GetDirectories(@"\\172.25.0.100\filesitgenebank$\");
                /*string[] theFolders = System.IO.Directory.GetDirectories(@"\\cip1256\public\");
                MessageBox.Show(theFolders.Length.ToString());*/
            }
            catch (Win32Exception ex2)
            {
                MessageBox.Show(ex2.Message + getErrorMessage(ex2.NativeErrorCode));
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        [DllImport("coredll.dll")]
        static extern uint FormatMessage(uint dwFlags, IntPtr lpSource, uint dwMessageId, uint dwLanguageId
            , [Out] StringBuilder lpBuffer, uint nSize, IntPtr Arguments);

        private string getErrorMessage(int errorCode)
        {
            string errorMessage = string.Empty;
            StringBuilder msgBuilder = new StringBuilder(101);

            try
            {
                // from header files
                const uint FORMAT_MESSAGE_ALLOCATE_BUFFER = 0x00000100;
                const uint FORMAT_MESSAGE_IGNORE_INSERTS = 0x00000200;
                const uint FORMAT_MESSAGE_FROM_SYSTEM = 0x00001000;
                const uint FORMAT_MESSAGE_ARGUMENT_ARRAY = 0x00002000;
                const uint FORMAT_MESSAGE_FROM_HMODULE = 0x00000800;
                const uint FORMAT_MESSAGE_FROM_STRING = 0x00000400;

                int nLastError = errorCode;
                uint dwChars = FormatMessage(FORMAT_MESSAGE_FROM_SYSTEM, IntPtr.Zero, (uint)nLastError, 0, msgBuilder, 101, IntPtr.Zero);
                if (dwChars != 0)
                {
                    errorMessage = msgBuilder.ToString();
                }
            }
            catch
            {
                errorMessage = "***";
            }
            return errorMessage;
        }

        private void button_Exists_Click(object sender, EventArgs e)
        {
            try
            {
                string dir = listBox_URN.SelectedItem.ToString();
                if( File.Exists(dir))
                    MessageBox.Show("Existe");
                else
                    MessageBox.Show("NO Existe");
                
            }
            catch (Win32Exception ex2)
            {
                MessageBox.Show(ex2.Message + getErrorMessage(ex2.NativeErrorCode));
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void button_Download_Click(object sender, EventArgs e)
        {
            try
            {
                string URL = listBox_URN.SelectedItem.ToString();
                HttpWebRequest request = (HttpWebRequest)WebRequest.Create(URL);
                request.Method = "GET";

                Cursor.Current = Cursors.WaitCursor;
                string content = string.Empty;
                using (HttpWebResponse response = (HttpWebResponse)request.GetResponse())
                {
                    using (Stream s = response.GetResponseStream())
                    {
                        using (StreamReader sr = new StreamReader(s, true))
                        {
                            content = sr.ReadToEnd();
                            MessageBox.Show(content);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally {
                Cursor.Current = Cursors.Default;
            }
        }
    }
}