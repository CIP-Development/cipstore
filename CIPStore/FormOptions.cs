﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace CIPStore
{
    public partial class FormOptions : Form
    {
        public FormOptions()
        {
            InitializeComponent();
        }

        private void button_Update_Click(object sender, EventArgs e)
        {
            //string strPathCurrent;
            string sourcePath, targetPath;

            //strPathCurrent = System.Reflection.Assembly.GetExecutingAssembly().GetName().CodeBase;
            //strPathCurrent = strPathCurrent.Substring(0, strPathCurrent.LastIndexOf("\\") + 1);

            sourcePath = textBox_ServerPath.Text + textBox_FilePath.Text;
            targetPath = textBox_AppInstallationPath.Text + textBox_LocalFilePath.Text;

            try
            {
                System.IO.File.Copy(sourcePath, targetPath, true);
                MessageBox.Show("App was updated");
            }
            catch (Exception ex)
            {
                MessageBox.Show("Try rebooting the device\n\n" + ex.Message);
            }
        }
    }
}