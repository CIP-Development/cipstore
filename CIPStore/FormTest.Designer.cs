﻿namespace CIPStore
{
    partial class FormTest
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;
        private System.Windows.Forms.MainMenu mainMenu1;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.mainMenu1 = new System.Windows.Forms.MainMenu();
            this.listBox_URN = new System.Windows.Forms.ListBox();
            this.button_Connect = new System.Windows.Forms.Button();
            this.button_Exists = new System.Windows.Forms.Button();
            this.button_Download = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // listBox_URN
            // 
            this.listBox_URN.Dock = System.Windows.Forms.DockStyle.Top;
            this.listBox_URN.Items.Add("\\\\cip1256\\public\\");
            this.listBox_URN.Items.Add("\\\\cip-docs\\FilesITGenebank$\\");
            this.listBox_URN.Items.Add("\\\\w7p-erojas\\sistemas$\\CIPTCLPublish\\");
            this.listBox_URN.Items.Add("\\\\cipstore\\public\\");
            this.listBox_URN.Items.Add("http://10.10.10.20/Webservices/CIPUpdater/apps_pocket.json");
            this.listBox_URN.Location = new System.Drawing.Point(0, 0);
            this.listBox_URN.Name = "listBox_URN";
            this.listBox_URN.Size = new System.Drawing.Size(240, 86);
            this.listBox_URN.TabIndex = 0;
            // 
            // button_Connect
            // 
            this.button_Connect.Location = new System.Drawing.Point(4, 237);
            this.button_Connect.Name = "button_Connect";
            this.button_Connect.Size = new System.Drawing.Size(72, 20);
            this.button_Connect.TabIndex = 1;
            this.button_Connect.Text = "connect";
            this.button_Connect.Click += new System.EventHandler(this.button_Connect_Click);
            // 
            // button_Exists
            // 
            this.button_Exists.Location = new System.Drawing.Point(82, 237);
            this.button_Exists.Name = "button_Exists";
            this.button_Exists.Size = new System.Drawing.Size(72, 20);
            this.button_Exists.TabIndex = 2;
            this.button_Exists.Text = "Exist?";
            this.button_Exists.Click += new System.EventHandler(this.button_Exists_Click);
            // 
            // button_Download
            // 
            this.button_Download.Location = new System.Drawing.Point(160, 237);
            this.button_Download.Name = "button_Download";
            this.button_Download.Size = new System.Drawing.Size(72, 20);
            this.button_Download.TabIndex = 3;
            this.button_Download.Text = "Download";
            this.button_Download.Click += new System.EventHandler(this.button_Download_Click);
            // 
            // FormTest
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.AutoScroll = true;
            this.ClientSize = new System.Drawing.Size(240, 268);
            this.Controls.Add(this.button_Download);
            this.Controls.Add(this.button_Exists);
            this.Controls.Add(this.button_Connect);
            this.Controls.Add(this.listBox_URN);
            this.Menu = this.mainMenu1;
            this.Name = "FormTest";
            this.Text = "FormTest";
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.ListBox listBox_URN;
        private System.Windows.Forms.Button button_Connect;
        private System.Windows.Forms.Button button_Exists;
        private System.Windows.Forms.Button button_Download;
    }
}