﻿namespace CIPStore
{
    partial class FormStore
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;
        private System.Windows.Forms.MainMenu mainMenu1;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.mainMenu1 = new System.Windows.Forms.MainMenu();
            this.menuItem_Refresh = new System.Windows.Forms.MenuItem();
            this.menuItem_Tools = new System.Windows.Forms.MenuItem();
            this.menuItem_SharedFolders = new System.Windows.Forms.MenuItem();
            this.menuItem_Options = new System.Windows.Forms.MenuItem();
            this.menuItem_CopyWithCred = new System.Windows.Forms.MenuItem();
            this.menuItem_Test = new System.Windows.Forms.MenuItem();
            this.menuItem_Servers = new System.Windows.Forms.MenuItem();
            this.label_ServerPath = new System.Windows.Forms.Label();
            this.appCard_temp = new CIPStore.AppCard();
            this.SuspendLayout();
            // 
            // mainMenu1
            // 
            this.mainMenu1.MenuItems.Add(this.menuItem_Refresh);
            this.mainMenu1.MenuItems.Add(this.menuItem_Tools);
            // 
            // menuItem_Refresh
            // 
            this.menuItem_Refresh.Text = "Refrescar";
            this.menuItem_Refresh.Click += new System.EventHandler(this.menuItem_Refresh_Click);
            // 
            // menuItem_Tools
            // 
            this.menuItem_Tools.MenuItems.Add(this.menuItem_SharedFolders);
            this.menuItem_Tools.MenuItems.Add(this.menuItem_Options);
            this.menuItem_Tools.MenuItems.Add(this.menuItem_CopyWithCred);
            this.menuItem_Tools.MenuItems.Add(this.menuItem_Test);
            this.menuItem_Tools.MenuItems.Add(this.menuItem_Servers);
            this.menuItem_Tools.Text = "Herramientas";
            // 
            // menuItem_SharedFolders
            // 
            this.menuItem_SharedFolders.Text = "Carpeta Compartida";
            this.menuItem_SharedFolders.Click += new System.EventHandler(this.menuItem_SharedFolders_Click);
            // 
            // menuItem_Options
            // 
            this.menuItem_Options.Text = "Opciones";
            this.menuItem_Options.Click += new System.EventHandler(this.menuItem_Options_Click);
            // 
            // menuItem_CopyWithCred
            // 
            this.menuItem_CopyWithCred.Enabled = false;
            this.menuItem_CopyWithCred.Text = "CopyWithCred";
            this.menuItem_CopyWithCred.Click += new System.EventHandler(this.menuItem_CopyWithCred_Click);
            // 
            // menuItem_Test
            // 
            this.menuItem_Test.Enabled = false;
            this.menuItem_Test.Text = "Test";
            this.menuItem_Test.Click += new System.EventHandler(this.menuItem_Test_Click);
            // 
            // menuItem_Servers
            // 
            this.menuItem_Servers.Text = "Servidores";
            this.menuItem_Servers.Click += new System.EventHandler(this.menuItem_Servers_Click);
            // 
            // label_ServerPath
            // 
            this.label_ServerPath.BackColor = System.Drawing.Color.Gainsboro;
            this.label_ServerPath.Dock = System.Windows.Forms.DockStyle.Top;
            this.label_ServerPath.Location = new System.Drawing.Point(0, 0);
            this.label_ServerPath.Name = "label_ServerPath";
            this.label_ServerPath.Size = new System.Drawing.Size(240, 20);
            this.label_ServerPath.Text = "label1";
            // 
            // appCard_temp
            // 
            this.appCard_temp.AppLastVersion = "LastVersion";
            this.appCard_temp.AppName = "AppName";
            this.appCard_temp.BackColor = System.Drawing.Color.Gainsboro;
            this.appCard_temp.Dock = System.Windows.Forms.DockStyle.Top;
            this.appCard_temp.Location = new System.Drawing.Point(0, 20);
            this.appCard_temp.Name = "appCard_temp";
            this.appCard_temp.Size = new System.Drawing.Size(240, 65);
            this.appCard_temp.TabIndex = 11;
            // 
            // FormStore
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.AutoScroll = true;
            this.BackColor = System.Drawing.Color.Gainsboro;
            this.ClientSize = new System.Drawing.Size(240, 268);
            this.Controls.Add(this.appCard_temp);
            this.Controls.Add(this.label_ServerPath);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Menu = this.mainMenu1;
            this.MinimizeBox = false;
            this.Name = "FormStore";
            this.Text = "CIPStore";
            this.Load += new System.EventHandler(this.FormStore_Load);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.MenuItem menuItem_Refresh;
        private AppCard appCard_temp;
        private System.Windows.Forms.MenuItem menuItem_Tools;
        private System.Windows.Forms.MenuItem menuItem_SharedFolders;
        private System.Windows.Forms.MenuItem menuItem_Options;
        private System.Windows.Forms.MenuItem menuItem_CopyWithCred;
        private System.Windows.Forms.MenuItem menuItem_Test;
        private System.Windows.Forms.MenuItem menuItem_Servers;
        private System.Windows.Forms.Label label_ServerPath;
    }
}

