﻿namespace CIPStore
{
    partial class FormOptions
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;
        private System.Windows.Forms.MainMenu mainMenu1;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.mainMenu1 = new System.Windows.Forms.MainMenu();
            this.panel_Temp = new System.Windows.Forms.Panel();
            this.Label_Server = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.textBox_ServerPath = new System.Windows.Forms.TextBox();
            this.textBox_LocalFilePath = new System.Windows.Forms.TextBox();
            this.button_Update = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.textBox_FilePath = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.textBox_AppInstallationPath = new System.Windows.Forms.TextBox();
            this.panel_Temp.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel_Temp
            // 
            this.panel_Temp.BackColor = System.Drawing.Color.LightYellow;
            this.panel_Temp.Controls.Add(this.Label_Server);
            this.panel_Temp.Controls.Add(this.label3);
            this.panel_Temp.Controls.Add(this.textBox_ServerPath);
            this.panel_Temp.Controls.Add(this.textBox_LocalFilePath);
            this.panel_Temp.Controls.Add(this.button_Update);
            this.panel_Temp.Controls.Add(this.label2);
            this.panel_Temp.Controls.Add(this.textBox_FilePath);
            this.panel_Temp.Controls.Add(this.label1);
            this.panel_Temp.Controls.Add(this.textBox_AppInstallationPath);
            this.panel_Temp.Location = new System.Drawing.Point(3, 3);
            this.panel_Temp.Name = "panel_Temp";
            this.panel_Temp.Size = new System.Drawing.Size(224, 238);
            // 
            // Label_Server
            // 
            this.Label_Server.Location = new System.Drawing.Point(3, 13);
            this.Label_Server.Name = "Label_Server";
            this.Label_Server.Size = new System.Drawing.Size(210, 20);
            this.Label_Server.Text = "Server Path";
            // 
            // label3
            // 
            this.label3.Location = new System.Drawing.Point(3, 159);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(210, 20);
            this.label3.Text = "Local File Path";
            // 
            // textBox_ServerPath
            // 
            this.textBox_ServerPath.Location = new System.Drawing.Point(3, 37);
            this.textBox_ServerPath.Name = "textBox_ServerPath";
            this.textBox_ServerPath.Size = new System.Drawing.Size(210, 21);
            this.textBox_ServerPath.TabIndex = 1;
            this.textBox_ServerPath.Text = "\\\\w7p-erojas\\sistemas$\\CIPTCLPublish\\";
            // 
            // textBox_LocalFilePath
            // 
            this.textBox_LocalFilePath.Location = new System.Drawing.Point(3, 182);
            this.textBox_LocalFilePath.Name = "textBox_LocalFilePath";
            this.textBox_LocalFilePath.Size = new System.Drawing.Size(210, 21);
            this.textBox_LocalFilePath.TabIndex = 7;
            this.textBox_LocalFilePath.Text = "CIPTCLPocket2.exe";
            // 
            // button_Update
            // 
            this.button_Update.Location = new System.Drawing.Point(3, 209);
            this.button_Update.Name = "button_Update";
            this.button_Update.Size = new System.Drawing.Size(72, 20);
            this.button_Update.TabIndex = 2;
            this.button_Update.Text = "Update";
            this.button_Update.Click += new System.EventHandler(this.button_Update_Click);
            // 
            // label2
            // 
            this.label2.Location = new System.Drawing.Point(3, 112);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(210, 20);
            this.label2.Text = "App Installation Path";
            // 
            // textBox_FilePath
            // 
            this.textBox_FilePath.Location = new System.Drawing.Point(3, 84);
            this.textBox_FilePath.Name = "textBox_FilePath";
            this.textBox_FilePath.Size = new System.Drawing.Size(210, 21);
            this.textBox_FilePath.TabIndex = 3;
            this.textBox_FilePath.Text = "ciptclPocket\\CIPTCLPocket2.exe";
            // 
            // label1
            // 
            this.label1.Location = new System.Drawing.Point(4, 62);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(209, 20);
            this.label1.Text = "File Path";
            // 
            // textBox_AppInstallationPath
            // 
            this.textBox_AppInstallationPath.Location = new System.Drawing.Point(3, 135);
            this.textBox_AppInstallationPath.Name = "textBox_AppInstallationPath";
            this.textBox_AppInstallationPath.Size = new System.Drawing.Size(210, 21);
            this.textBox_AppInstallationPath.TabIndex = 4;
            this.textBox_AppInstallationPath.Text = "\\Program Files\\ciptclpocket\\";
            // 
            // FormOptions
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.AutoScroll = true;
            this.ClientSize = new System.Drawing.Size(240, 268);
            this.Controls.Add(this.panel_Temp);
            this.Menu = this.mainMenu1;
            this.Name = "FormOptions";
            this.Text = "FormOptions";
            this.panel_Temp.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel_Temp;
        private System.Windows.Forms.Label Label_Server;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox textBox_ServerPath;
        private System.Windows.Forms.TextBox textBox_LocalFilePath;
        private System.Windows.Forms.Button button_Update;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox textBox_FilePath;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox textBox_AppInstallationPath;
    }
}