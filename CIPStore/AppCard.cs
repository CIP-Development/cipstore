﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;

namespace CIPStore
{
    public enum AppStatusEnum
    {
        error = 0,
        not_installed = 1,
        updated = 2,
        outdated = 3,
        forced_update = 4
    }
    public partial class AppCard : UserControl
    {
        private AppStatusEnum _appStatus;

        public AppCard()
        {
            InitializeComponent();
        }

        //[System.ComponentModel.Description("Gets or sets the app name."), System.ComponentModel.Category("AppCard")]
        public string AppName
        {
            get
            {
                return label_AppName.Text;
            }
            set
            {
                label_AppName.Text = value;
            }
        }

        //[System.ComponentModel.Description("Gets or sets the last version."), System.ComponentModel.Category("AppCard")]
        public string AppLastVersion
        {
            get
            {
                return label_LastVersion.Text;
            }
            set
            {
                label_LastVersion.Text = value;
            }
        }

        //[System.ComponentModel.Description("Gets or sets the app icon."), System.ComponentModel.Category("AppCard")]
        public Image AppIcon
        {
            get
            {
                return pictureBox_Icon.Image;
            }
            set
            {
                pictureBox_Icon.Image = value;
            }
        }

        //[System.ComponentModel.Description("Gets or sets the app status."), System.ComponentModel.Category("AppCard")]
        public AppStatusEnum AppStatus
        {
            get
            {
                return _appStatus;
            }
            set
            {
                _appStatus = value;
                switch (_appStatus)
                {
                    case AppStatusEnum.not_installed:
                        label_Status.ForeColor = Color.Black;
                        label_Status.Text = "No instalado";
                        button_Action.Visible = true;
                        button_Action.Text = "Instalar";
                        button_Action.BackColor = Color.SkyBlue;
                        break;
                    case AppStatusEnum.forced_update:
                        label_Status.ForeColor = Color.Red;
                        label_Status.Text = "Act. forzada";
                        button_Action.Visible = true;
                        button_Action.Text = "Actualizar";
                        button_Action.BackColor = Color.YellowGreen;
                        break;
                    case AppStatusEnum.outdated:
                        label_Status.ForeColor = Color.Red;
                        label_Status.Text = "Desactualizado";
                        button_Action.Visible = true;
                        button_Action.Text = "Actualizar";
                        button_Action.BackColor = Color.YellowGreen;
                        break;
                    case AppStatusEnum.updated:
                        label_Status.ForeColor = Color.Green;
                        label_Status.Text = "Actualizado";
                        button_Action.Visible = true;
                        button_Action.Text = "Abrir";
                        button_Action.BackColor = Color.YellowGreen;
                        break;
                    case AppStatusEnum.error:
                        label_Status.ForeColor = Color.Red;
                        label_Status.Text = "Error";
                        button_Action.Visible = false;
                        break;
                    default:
                        label_Status.ForeColor = Color.Black;
                        button_Action.Visible = false;
                        break;
                }
            }
        }

        public EventHandler ActionClick
        {
            get
            {
                throw new System.NotImplementedException();
            }
            set
            {
                button_Action.Click += value;
            }
        }

        public string AppId { get; set; }
    }
}
