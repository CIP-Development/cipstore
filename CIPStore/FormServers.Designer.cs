﻿namespace CIPStore
{
    partial class FormServers
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;
        private System.Windows.Forms.MainMenu mainMenu1;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.mainMenu1 = new System.Windows.Forms.MainMenu();
            this.listBox_Servers = new System.Windows.Forms.ListBox();
            this.button_Select = new System.Windows.Forms.Button();
            this.button_Cancel = new System.Windows.Forms.Button();
            this.button_TestConnection = new System.Windows.Forms.Button();
            this.webBrowser1 = new System.Windows.Forms.WebBrowser();
            this.SuspendLayout();
            // 
            // listBox_Servers
            // 
            this.listBox_Servers.Dock = System.Windows.Forms.DockStyle.Top;
            this.listBox_Servers.Items.Add("http://190.216.179.104/Webservices/CIPUpdater/");
            this.listBox_Servers.Items.Add("http://10.16.0.20/Webservices/CIPUpdater/");
            this.listBox_Servers.Items.Add("http://cipweb2/Webservices/CIPUpdater/");
            this.listBox_Servers.Items.Add("http://genebank.cipotato.org/Webservices/CIPUpdater/");
            this.listBox_Servers.Location = new System.Drawing.Point(0, 0);
            this.listBox_Servers.Name = "listBox_Servers";
            this.listBox_Servers.Size = new System.Drawing.Size(240, 72);
            this.listBox_Servers.TabIndex = 0;
            // 
            // button_Select
            // 
            this.button_Select.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.button_Select.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Bold);
            this.button_Select.Location = new System.Drawing.Point(3, 233);
            this.button_Select.Name = "button_Select";
            this.button_Select.Size = new System.Drawing.Size(128, 30);
            this.button_Select.TabIndex = 1;
            this.button_Select.Text = "Cambiar servidor";
            this.button_Select.Click += new System.EventHandler(this.button_Select_Click);
            // 
            // button_Cancel
            // 
            this.button_Cancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.button_Cancel.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Bold);
            this.button_Cancel.Location = new System.Drawing.Point(137, 233);
            this.button_Cancel.Name = "button_Cancel";
            this.button_Cancel.Size = new System.Drawing.Size(100, 30);
            this.button_Cancel.TabIndex = 2;
            this.button_Cancel.Text = "Cancelar";
            this.button_Cancel.Click += new System.EventHandler(this.button_Cancel_Click);
            // 
            // button_TestConnection
            // 
            this.button_TestConnection.Dock = System.Windows.Forms.DockStyle.Top;
            this.button_TestConnection.Location = new System.Drawing.Point(0, 72);
            this.button_TestConnection.Name = "button_TestConnection";
            this.button_TestConnection.Size = new System.Drawing.Size(240, 30);
            this.button_TestConnection.TabIndex = 3;
            this.button_TestConnection.Text = "Probar conexión";
            this.button_TestConnection.Click += new System.EventHandler(this.button_TestConnection_Click);
            // 
            // webBrowser1
            // 
            this.webBrowser1.Dock = System.Windows.Forms.DockStyle.Top;
            this.webBrowser1.Location = new System.Drawing.Point(0, 102);
            this.webBrowser1.Name = "webBrowser1";
            this.webBrowser1.Size = new System.Drawing.Size(240, 125);
            // 
            // FormServers
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.AutoScroll = true;
            this.ClientSize = new System.Drawing.Size(240, 268);
            this.Controls.Add(this.webBrowser1);
            this.Controls.Add(this.button_TestConnection);
            this.Controls.Add(this.button_Cancel);
            this.Controls.Add(this.button_Select);
            this.Controls.Add(this.listBox_Servers);
            this.Menu = this.mainMenu1;
            this.Name = "FormServers";
            this.Text = "FormServers";
            this.Load += new System.EventHandler(this.FormServers_Load);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.ListBox listBox_Servers;
        private System.Windows.Forms.Button button_Select;
        private System.Windows.Forms.Button button_Cancel;
        private System.Windows.Forms.Button button_TestConnection;
        private System.Windows.Forms.WebBrowser webBrowser1;
    }
}