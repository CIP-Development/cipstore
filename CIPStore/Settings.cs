﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;

namespace CIPStore
{
    public interface ISettings
    {
        List<string> Servers { get; set; }

        /// <summary>
        /// Save settings permanently
        /// </summary>
        /// <returns>return true if settings were saved successfully</returns>
        bool Save();
    }

    public class Settings : ISettings  
    {
        private static Settings _instance = null;
        public static Settings Instance
        {
            get
            {
                if (_instance == null)
                    _instance = new Settings();
                return _instance;
            }
        }

        public Settings() 
        {
            Servers = new List<string>() { "http://10.16.0.20/Webservices/CIPUpdater/"
                    , "http://190.216.179.104/Webservices/CIPUpdater/"
                    , "http://cipweb2/Webservices/CIPUpdater/"
                    , "http://genebank.cipotato.org/Webservices/CIPUpdater/"};
        }

        public List<string> Servers { get; set; }

        public bool Save()
        {
            return true;
        }
        /// <summary>
        /// Load settings from app config file
        /// </summary>
        private void Load()
        {
        }
    }
}
