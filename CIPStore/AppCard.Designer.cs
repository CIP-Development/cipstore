﻿namespace CIPStore
{
    partial class AppCard
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.pictureBox_Icon = new System.Windows.Forms.PictureBox();
            this.label_AppName = new System.Windows.Forms.Label();
            this.button_Action = new System.Windows.Forms.Button();
            this.label_Status = new System.Windows.Forms.Label();
            this.label_LastVersion = new System.Windows.Forms.Label();
            this.panel_Card = new System.Windows.Forms.Panel();
            this.panel_Card.SuspendLayout();
            this.SuspendLayout();
            // 
            // pictureBox_Icon
            // 
            this.pictureBox_Icon.Location = new System.Drawing.Point(8, 18);
            this.pictureBox_Icon.Name = "pictureBox_Icon";
            this.pictureBox_Icon.Size = new System.Drawing.Size(32, 32);
            this.pictureBox_Icon.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            // 
            // label_AppName
            // 
            this.label_AppName.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.label_AppName.Location = new System.Drawing.Point(46, 11);
            this.label_AppName.Name = "label_AppName";
            this.label_AppName.Size = new System.Drawing.Size(160, 15);
            this.label_AppName.Text = "AppName";
            // 
            // button_Action
            // 
            this.button_Action.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.button_Action.BackColor = System.Drawing.Color.YellowGreen;
            this.button_Action.ForeColor = System.Drawing.Color.Black;
            this.button_Action.Location = new System.Drawing.Point(142, 34);
            this.button_Action.Name = "button_Action";
            this.button_Action.Size = new System.Drawing.Size(71, 20);
            this.button_Action.TabIndex = 2;
            this.button_Action.Text = "Action";
            this.button_Action.Visible = false;
            // 
            // label_Status
            // 
            this.label_Status.Location = new System.Drawing.Point(46, 41);
            this.label_Status.Name = "label_Status";
            this.label_Status.Size = new System.Drawing.Size(90, 15);
            this.label_Status.Text = "Status";
            // 
            // label_LastVersion
            // 
            this.label_LastVersion.Location = new System.Drawing.Point(46, 26);
            this.label_LastVersion.Name = "label_LastVersion";
            this.label_LastVersion.Size = new System.Drawing.Size(90, 15);
            this.label_LastVersion.Text = "LastVersion";
            // 
            // panel_Card
            // 
            this.panel_Card.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.panel_Card.BackColor = System.Drawing.Color.White;
            this.panel_Card.Controls.Add(this.button_Action);
            this.panel_Card.Controls.Add(this.label_AppName);
            this.panel_Card.Controls.Add(this.pictureBox_Icon);
            this.panel_Card.Controls.Add(this.label_Status);
            this.panel_Card.Controls.Add(this.label_LastVersion);
            this.panel_Card.Location = new System.Drawing.Point(8, 0);
            this.panel_Card.Name = "panel_Card";
            this.panel_Card.Size = new System.Drawing.Size(224, 64);
            // 
            // AppCard
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.BackColor = System.Drawing.Color.Gainsboro;
            this.Controls.Add(this.panel_Card);
            this.Name = "AppCard";
            this.Size = new System.Drawing.Size(240, 65);
            this.panel_Card.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.PictureBox pictureBox_Icon;
        private System.Windows.Forms.Label label_AppName;
        private System.Windows.Forms.Button button_Action;
        private System.Windows.Forms.Label label_Status;
        private System.Windows.Forms.Label label_LastVersion;
        private System.Windows.Forms.Panel panel_Card;
    }
}
