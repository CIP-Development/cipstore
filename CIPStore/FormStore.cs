﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.IO;
using Newtonsoft.Json;
using System.Reflection;
using System.Threading;
using System.Diagnostics;
using System.Net;
using System.Runtime.InteropServices;
using Microsoft.Win32;
using System.Net.Sockets;

namespace CIPStore
{
    public partial class FormStore : Form
    {
        private ISettings _settings;
        private List<App> _appList;
        private List<AppCard> _appCardList;
        
        private string _serverPath;
        private string _guid;
        private Version _version;
        private string _currentDirectory;
        private string _programFilesDirectory;
        StringBuilder sPath = new StringBuilder(50);
        bool ret;

        private string ServerPath
        {
            get
            {
                return _serverPath;
            }
            set
            {
                _serverPath = value;
                label_ServerPath.Text = value;
            }
        }

        [DllImport("coredll.dll", EntryPoint = "SHGetSpecialFolderPath")]
        static extern bool SHGetSpecialFolderPath(IntPtr hwndOwner, StringBuilder lpszPath, int nFolder, bool fCreate);
        
        public FormStore()
        {
            InitializeComponent();
            _appCardList = new List<AppCard>();
            _settings = Settings.Instance;

            //Get GUID and version of CIPStore
            var assembly = typeof(Program).Assembly;
            var attribute = (GuidAttribute)assembly.GetCustomAttributes(typeof(GuidAttribute),true)[0];
            _guid = attribute.Value;
            _version = System.Reflection.Assembly.GetExecutingAssembly().GetName().Version;

            var assemblyLocation = Assembly.GetExecutingAssembly().GetName().CodeBase;
            _currentDirectory = Path.GetDirectoryName(assemblyLocation);

            const int CSIDL_PROGRAM_FILES = 0x26;

            ret = SHGetSpecialFolderPath(IntPtr.Zero, sPath, CSIDL_PROGRAM_FILES, false);
            _programFilesDirectory = sPath.ToString();
        }

        private void menuItem_Refresh_Click(object sender, EventArgs e)
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;

                //Clean list
                foreach (var appCard in _appCardList)
                {
                    this.Controls.Remove(appCard);
                }

                string sourcePath = _serverPath + "apps_pocket2.json";
                string appsJSON = string.Empty;

                //Get App Info file
                HttpWebRequest request = (HttpWebRequest)WebRequest.Create(sourcePath);
                using (HttpWebResponse response = (HttpWebResponse)request.GetResponse())
                {
                    using (Stream s = response.GetResponseStream())
                    {
                        using (StreamReader sr = new StreamReader(s, true))
                        {
                            appsJSON = sr.ReadToEnd();
                        }
                    }
                }
                /*using (var sr = new StreamReader(sourcePath, Encoding.UTF8))
                {
                    appsJSON = sr.ReadToEnd();
                }*/

                if (string.IsNullOrEmpty(appsJSON))
                {
                    throw new Exception("Empty file apps.json");
                }
                //Get App List
                _appList = JsonConvert.DeserializeObject<List<App>>(appsJSON);

                //Check if CIPStore needs update
                int iCipStore = _appList.FindIndex(x => x.AppId.Equals(_guid));
                if (iCipStore >= 0)
                {
                    if (_version < new Version(_appList[iCipStore].LastVersion))
                    {
                        try
                        {
                            if (MessageBox.Show("Hay una actualización de CIPStore\n\nDesea instalar la nueva versión?", "Nueva versión encontrada"
                                        , MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button1) == DialogResult.Yes)
                            {
                                updateApp(_appList[iCipStore]);
                            }
                        }
                        catch (Exception ex)
                        {
                            MessageBox.Show("Ocurrió un error al intentar instalar la nueva versión de CIPStore\n\n" + ex.Message, "Error"
                                        , MessageBoxButtons.OK, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button1);
                        }
                    }
                    _appList.RemoveAt(iCipStore);
                }

                //Update AppCardList
                foreach (var app in _appList)
                {
                    if (File.Exists(app.DefaultInstallPath + app.DefaultVersionPath))
                    {
                        try
                        {
                            if (app.ForcedUpdate)
                            {
                                app.Status = AppStatusEnum.forced_update;
                                app.Order = 0;
                            }
                            else
                            {
                                if (!File.Exists(app.DefaultInstallPath + "version.txt"))
                                {
                                    app.Status = AppStatusEnum.outdated;
                                    app.Order = 1;
                                }
                                else
                                {
                                    Version version;
                                    using (StreamReader sr = File.OpenText(app.DefaultInstallPath + "version.txt"))
                                    {
                                        version = new Version(sr.ReadToEnd());
                                    }
                                    var lastVersion = new Version(app.LastVersion);
                                    if (version < lastVersion)
                                    {
                                        app.Status = AppStatusEnum.outdated;
                                        app.Order = 1;
                                        //app.LastVersion += " / " + version.ToString();
                                    }
                                    else
                                    {
                                        app.Status = AppStatusEnum.updated;
                                        app.Order = 3;
                                    }
                                    app.CurrentVersion = version.ToString();
                                }
                            }
                        }
                        catch(Exception ex)
                        {
                            app.Status = AppStatusEnum.error;
                            app.Order = 2;
                        }
                    }
                    else
                    {
                        app.Status = AppStatusEnum.not_installed;
                        app.Order = 4;
                    }

                    //Icon
                    if (!string.IsNullOrEmpty(app.AppIcon))
                    {
                        try
                        {
                            string iconPath = _serverPath + app.AppIcon;
                            Bitmap img;
                            Icon icon;

                            HttpWebRequest requestIcon = (HttpWebRequest)WebRequest.Create(iconPath);
                            using (HttpWebResponse response = (HttpWebResponse)requestIcon.GetResponse())
                            {
                                using (Stream s = response.GetResponseStream())
                                {
                                    icon = new Icon(s);
                                }
                            }
                            /* using (var fs = new FileStream(iconPath, FileMode.Open, FileAccess.Read))
                            {
                                icon = new Icon(fs);
                            }*/
                            img = new Bitmap(icon.Width, icon.Height);
                            Graphics g = Graphics.FromImage(img);
                            g.DrawIcon(icon, 0, 0);
                            g.Dispose();

                            app.Icon = img;
                        }
                        catch (Exception ex1)
                        {
                            MessageBox.Show(ex1.Message);
                        }
                    }
                }

                _appList.Sort(new sortAppHelper());

                foreach (var app in _appList)
                {
                    AppCard appCard = new AppCard()
                    {
                        AppId = app.AppId,
                        AppName = app.AppName,
                        AppLastVersion = app.LastVersion,
                        AppStatus = app.Status,
                        Location = new System.Drawing.Point(0, 0),
                        Name = "appCard_" + app.AppId,
                        Dock = DockStyle.Top,
                        AppIcon = app.Icon,
                        ActionClick = new System.EventHandler(appActionClick)
                    };
                    _appCardList.Add(appCard);
                    this.Controls.Add(appCard);
                    appCard.BringToFront();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally {
                Cursor.Current = Cursors.Default;
            }
        }

        private void FormStore_Load(object sender, EventArgs e)
        {
            try
            {
                this.Controls.Remove(appCard_temp);

                if (_settings.Servers != null && _settings.Servers.Count > 0)
                {
                    //search for a server
                    
                    /*bool isOK = false;
                    foreach (var server in _settings.Servers)
                    {
                        ServerPath = server;
                        menuItem_Refresh_Click(null, null);
                        break;
                    }*/
                    ServerPath = _settings.Servers[0];
                    menuItem_Refresh_Click(null, null);
                }
                else
                {
                    ServerPath = @"http://10.16.0.20/Webservices/CIPUpdater/";
                    menuItem_Refresh_Click(null, null);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            
        }

        private void appActionClick(object sender, EventArgs e)
        {
            var appCard = (AppCard)((Button)sender).Parent.Parent;
            var app = _appList.Where(x => x.AppId.Equals(appCard.AppId)).FirstOrDefault();

            string sourcePath, targetPath, tempPath;
            sourcePath = _serverPath + app.LastVersionPath;
            targetPath = app.DefaultInstallPath + app.DefaultVersionPath;
            string versionPath = app.DefaultInstallPath + "version.txt";
            try
            {
                Cursor.Current = Cursors.WaitCursor;

                switch (app.Status)
                {
                    case AppStatusEnum.not_installed:
                        updateApp(app);
                        break;
                    case AppStatusEnum.updated:
                        Process pApp = new System.Diagnostics.Process();
                        pApp.StartInfo.FileName = app.DefaultInstallPath + app.DefaultVersionPath;
                        pApp.StartInfo.UseShellExecute = false;
                        pApp.Start();
                        this.Close();
                        break;
                    case AppStatusEnum.outdated:
                    case AppStatusEnum.forced_update:
                        //Check if LastVersionFiles has values
                        if (app.LastVersionFiles != null && app.LastVersionFiles.Count > 0)
                        {
                            //download files in temp folder
                            string assemblyLocation = Assembly.GetExecutingAssembly().GetName().CodeBase;
                            string currentDirectory = Path.GetDirectoryName(assemblyLocation);
                            string tempDirectory = currentDirectory + "/temp";
                            if (!Directory.Exists(tempDirectory))
                            {
                                Directory.CreateDirectory(tempDirectory);
                            }
                            foreach (string lastVersionFile in app.LastVersionFiles)
                            {
                                sourcePath = _serverPath + app.LastVersionPath + "/" + lastVersionFile;
                                tempPath = tempDirectory + "/" + lastVersionFile;

                                //check if path exists for nested folder f.e. Res/*.*
                                if (!Directory.Exists(Path.GetDirectoryName(tempPath)))
                                {
                                    Directory.CreateDirectory(Path.GetDirectoryName(tempPath));
                                }
                                else
                                {
                                    if (File.Exists(tempPath))
                                    {
                                        File.Delete(tempPath);
                                    }
                                }

                                //If extension is not allowed add .txt
                                if(Path.GetExtension(sourcePath) == ".config")
                                    sourcePath += ".txt";
                                HttpWebRequest request = (HttpWebRequest)WebRequest.Create(sourcePath);
                                using (HttpWebResponse response = (HttpWebResponse)request.GetResponse())
                                {
                                    using (Stream sInput = response.GetResponseStream())
                                    using (Stream sOutput = File.Create(tempPath))
                                    {
                                        byte[] buffer = new byte[1024];
                                        int bytesRead;
                                        while ((bytesRead = sInput.Read(buffer, 0, buffer.Length)) > 0)
                                        {
                                            sOutput.Write(buffer, 0, bytesRead);
                                        }
                                    }
                                }
                            }
                            //replace outdated files
                            foreach (string lastVersionFile in app.LastVersionFiles)
                            {
                                tempPath = tempDirectory + "/" + lastVersionFile;
                                targetPath = app.DefaultInstallPath + lastVersionFile;

                                //check if path exists for nested folder
                                if (!Directory.Exists(Path.GetDirectoryName(targetPath)))
                                {
                                    Directory.CreateDirectory(Path.GetDirectoryName(targetPath));
                                }
                                else
                                {
                                    if (File.Exists(targetPath))
                                    {
                                        File.Delete(targetPath);
                                    }
                                }
                                File.Move(tempPath, targetPath);
                            }
                        }
                        else //LastVersionFiles is null or empty
                        {
                            throw new Exception("No se definieron los archivos de actualización para " + app.AppName + "\n\nLastVersionFiles is null or empty");
                        }
                        //Update version file
                        using (Stream sOutput = File.Create(versionPath))
                        {
                            using (StreamWriter sr = new StreamWriter(sOutput))
                            {
                                sr.Write(app.LastVersion);
                            }
                        }

                        MessageBox.Show("La aplicación ha sido actualizada.");
                        appCard.AppStatus = AppStatusEnum.updated;
                        app.Status = AppStatusEnum.updated;
                        break;
                    default:
                        throw new Exception("Action not allowed for status " + app.Status.ToString());
                }   
            }
            catch (System.IO.DirectoryNotFoundException ex)
            {
                MessageBox.Show("Directorio no encontrado.\n\n" + ex.Message);
            }
            catch (IOException ex)
            {
                MessageBox.Show("Intente después de reiniciar el dispositivo.\n\n" + ex.Message);
            }
            catch (System.Net.WebException ex)
            {
                MessageBox.Show("Error al descargar los archivos del servidor.\n\n" + ex.Message);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        private void updateApp(App app)
        { 
            //CIP Store
            //if(app.AppId.Equals(_guid))
            //{
                //Check LastInstaller
                if (string.IsNullOrEmpty(app.LastInstaller) == false)
                {
                    //Download cab installer in temp folder
                    string sourcePath = _serverPath + app.LastInstaller;
                    string tempPath = System.IO.Path.GetTempPath() + Path.GetFileName(app.LastInstaller);
                    
                    //check if tempPath exists
                    if (File.Exists(tempPath))
                    {
                        File.Delete(tempPath);
                    }

                    HttpWebRequest request = (HttpWebRequest)WebRequest.Create(sourcePath);
                    using (HttpWebResponse response = (HttpWebResponse)request.GetResponse())
                    {
                        using (Stream sInput = response.GetResponseStream())
                        using (Stream sOutput = File.Create(tempPath))
                        {
                            byte[] buffer = new byte[1024];
                            int bytesRead;
                            while ((bytesRead = sInput.Read(buffer, 0, buffer.Length)) > 0)
                            {
                                sOutput.Write(buffer, 0, bytesRead);
                            }
                        }
                    }
                    //Execute cab installer
                    Process pApp = new System.Diagnostics.Process();
                    pApp.StartInfo = new ProcessStartInfo("wceload", string.Format("\"{0}\"", tempPath));
                    pApp.StartInfo.UseShellExecute = false;
                    pApp.Start();

                    if (app.AppId.Equals(_guid))
                        Application.Exit();
                }
                else
                    throw new Exception("Archivo de instalación no definido\n\nLastInstaller is null or empty");
            //}
            //else
            //    throw new Exception("App Id no es el correcto");
        }

        private bool _eventHandled;
        private Process _myProcess;

        private void menuItem_GetAssemblyInfo_Click(object sender, EventArgs e)
        {

            generateVersionFile(@"\Program Files\ciptclpocket\", "CIPTCLPocket2.exe", "version.txt");

            return;

            _eventHandled = false;
            int elapsedTime = 0;
            try
            {
                _myProcess = new System.Diagnostics.Process();
                _myProcess.StartInfo.FileName = @"\Program Files\cipstore\AssemblyInfo.exe";
                _myProcess.StartInfo.UseShellExecute = false;
                _myProcess.StartInfo.Arguments = @"""\Program Files\ciptclpocket\"" ""CIPTCLPocket2.exe"" ""version.txt""";

                _myProcess.EnableRaisingEvents = true;
                _myProcess.Exited += new EventHandler(proc_Exited);

                _myProcess.Start();
                
                //proc0.WaitForExit();

                // Wait for Exited event, but not more than 5 seconds.
                int SLEEP_AMOUNT = 100;
                while (!_eventHandled)
                {
                    elapsedTime += SLEEP_AMOUNT;
                    if (elapsedTime > 5000)
                    {
                        _myProcess.Close();
                        break;
                    }
                    Thread.Sleep(SLEEP_AMOUNT);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally 
            {
                _myProcess.Dispose();
            }
        }

        private void proc_Exited(object sender, EventArgs e)
        {
            _eventHandled = true;
        }

        private void generateVersionFile(string installPath, string versionInput, string versionOutput)
        {
            bool isOK = false;
            int elapsedTime = 0;
            try
            {
                _myProcess = new System.Diagnostics.Process();
                _myProcess.StartInfo.FileName = @"\Program Files\cipstore\AssemblyInfo.exe";
                _myProcess.StartInfo.UseShellExecute = false;
                _myProcess.StartInfo.Arguments = string.Format(@"""{0}"" ""{1}"" ""{2}""", installPath, versionInput, versionOutput);

                _myProcess.Start();

                isOK = true;
                // Wait for Exited event, but not more than 5 seconds.
                const int SLEEP_AMOUNT = 100;
                while (!_myProcess.HasExited)
                {
                    elapsedTime += SLEEP_AMOUNT;
                    if (elapsedTime > 5000)
                    {
                        _myProcess.Close();
                        isOK = false;
                        break;
                    }
                    Thread.Sleep(SLEEP_AMOUNT);
                }
                if (isOK && _myProcess.ExitCode == 0)
                    MessageBox.Show("Ok");
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                //return false;
            }
            finally
            {
                _myProcess.Dispose();
            }
        }

        [DllImport("coredll.dll")]
        static extern uint FormatMessage(uint dwFlags, IntPtr lpSource, uint dwMessageId, uint dwLanguageId
            , [Out] StringBuilder lpBuffer, uint nSize, IntPtr Arguments);

        private string getErrorMessage(int errorCode)
        {
            string errorMessage = string.Empty;
            StringBuilder msgBuilder = new StringBuilder(101);

            try
            {
                // from header files
                const uint FORMAT_MESSAGE_ALLOCATE_BUFFER = 0x00000100;
                const uint FORMAT_MESSAGE_IGNORE_INSERTS = 0x00000200;
                const uint FORMAT_MESSAGE_FROM_SYSTEM = 0x00001000;
                const uint FORMAT_MESSAGE_ARGUMENT_ARRAY = 0x00002000;
                const uint FORMAT_MESSAGE_FROM_HMODULE = 0x00000800;
                const uint FORMAT_MESSAGE_FROM_STRING = 0x00000400;

                int nLastError = errorCode;
                uint dwChars = FormatMessage(FORMAT_MESSAGE_FROM_SYSTEM, IntPtr.Zero, (uint)nLastError, 0, msgBuilder, 101, IntPtr.Zero);
                if (dwChars != 0)
                {
                    errorMessage = msgBuilder.ToString();
                }
            }
            catch {
                errorMessage = "***";
            }
            return errorMessage;
        }

        private void menuItem_SharedFolders_Click(object sender, EventArgs e)
        {
            new FormTest().ShowDialog();
        }

        private void menuItem_Options_Click(object sender, EventArgs e)
        {
            new FormOptions().ShowDialog();
        }

        private void menuItem_CopyWithCred_Click(object sender, EventArgs e)
        {
            try
            {
                NetworkCredential cred = new NetworkCredential("cvelasquez", "", "cgiarad");
                using (new NetworkConnection(_serverPath, cred))
                {
                    File.Copy(_serverPath + "apps_pocket.json", @"\Program Files\cipstore\temp.json");
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void menuItem_Test_Click(object sender, EventArgs e)
        {
            try
            {
                //const string SERVER_IP = "172.25.12.94";
                const string SERVER_IP = "172.25.0.100";
                const int PORT_NO = 445;

                //---prepare the remote server address and port---
                IPAddress remoteAddress = IPAddress.Parse(SERVER_IP);
                IPEndPoint endPoint = new IPEndPoint(remoteAddress, PORT_NO);

                using (Socket client = new Socket(endPoint.AddressFamily, SocketType.Stream, ProtocolType.Tcp))
                {
                    //---connect to the server---
                    client.Connect(endPoint);

                    if (client.Connected == true) // Port is in use and connection is successful
                        MessageBox.Show("Port is open");

                    client.Close();
                }

                var ip = Dns.GetHostEntry("cipfs");
                MessageBox.Show(string.Format("{0},{1}", ip.HostName, ip.AddressList[0].ToString()));

                
                /*IntPtr hWnd = new IntPtr();

                NETRESOURCE ConnInf = new NETRESOURCE();
                ConnInf.dwType = RESOURCETYPE_ANY;
                ConnInf.RemoteName = @"\\172.25.12.94\";

                WNetAddConnection3(hWnd, ConnInf, null, null, CONNECT_INTERACTIVE | CONNECT_PROMPT);

                */
                //var result = PinvokeWindowsNetworking.connectToRemote(@"\\cip1256\public","cvelasquez", "");

                //MessageBox.Show(result);

                //string dir = @"\\172.25.0.100\public\";

                //RegistryKey rk = Registry.Users;

                const string machineRoot = "HKEY_LOCAL_MACHINE";
                const string subkey = @"Comm\Tcpip\Hosts\cipstore";
                const string keyName = machineRoot + "\\" + subkey;


                /*
                var data = "ac,19,0c,5e".Split(',').Select(x => Convert.ToByte(x, 16)).ToArray();
                Registry.SetValue(keyName, "ipaddr", data, RegistryValueKind.Binary);
                data = "99,99,99,99,99,99,99".Split(',').Select(x => Convert.ToByte(x, 16)).ToArray();
                Registry.SetValue(keyName, "ExpireTime", data, RegistryValueKind.Binary);
                */
                var datar = (byte[])Registry.GetValue(keyName, "ipaddr", new byte[0]);
                string ipaddr = string.Join(".", datar.Select(x => x.ToString("x2")).ToArray());

                datar = (byte[])Registry.GetValue(keyName, "ExpireTime", new byte[0]);
                string ExpireTime = string.Join(",", datar.Select(x => x.ToString("x2")).ToArray());
                MessageBox.Show(string.Format("ip: {0}\n expire: {1}", ipaddr, ExpireTime));

                //"ac.19.00.64 (0xac190064)"
                //string dir = @"\\cip1256\public\";
                string dir = @"\\w7p-erojas\sistemas$\CIPTCLPublish";
                NetworkConnection conn;
                //NetworkCredential cred = new NetworkCredential("Guest", "eVDmCR3rjtgyWr5A", "");
                NetworkCredential cred = new NetworkCredential("cvelasquez", "", "cgiarad");
                using (conn = new NetworkConnection(dir, cred))
                {
                    string[] fileEntries = Directory.GetFiles(dir);
                    MessageBox.Show(fileEntries.Length.ToString());
                    /*foreach (string file in fileEntries)
                    {
                        textbox.Text += file + "\r\n";
                    }*/
                }
                //Registry.CurrentUser.DeleteSubKey(subkey);

                /*NetworkCredential cred = new NetworkCredential("cvelasquez", "", "cgiarad");
                CredentialCache theNetcache = new CredentialCache();
                Uri resource = new Uri(@"\\172.25.0.100");
                theNetcache.Add(resource, "Basic", cred);
                Uri resource2 = new Uri(@"\\cip1256");
                theNetcache.Add(resource2, "NTLM", cred);*/

                //string[] theFolders = System.IO.Directory.GetDirectories(@"\\172.25.0.100\filesitgenebank$\");
                /*string[] theFolders = System.IO.Directory.GetDirectories(@"\\cip1256\public\");
                MessageBox.Show(theFolders.Length.ToString());*/
            }
            catch (Win32Exception ex2)
            {
                MessageBox.Show(ex2.Message + getErrorMessage(ex2.NativeErrorCode));
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void menuItem_Servers_Click(object sender, EventArgs e)
        {
            using(var form = new FormServers())
            {
                var result = form.ShowDialog();
                if(result == DialogResult.OK)
                {
                    ServerPath = form.ServerPath;
                }
            }
        }

    }
}